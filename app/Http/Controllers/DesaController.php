<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\desa;
use App\Models\kecamatan;

class DesaController extends Controller
{
    public function index()
    {
        $data = DB::table('desa')
                ->join('kecamatan','kecamatan.id','=','desa.id_kecamatan')
                ->select('desa.*','kecamatan.nama as kecamatan')
                ->orderBy('desa.id','desc')
                ->get();
        $kecamatan = kecamatan::all();
        return view('page.desa.index', compact('data','kecamatan'));
    }

    public function show(Request $request)
    {
        # code...
    }

    public function create(Request $request)
    {
        # code...
    }

    public function store(Request $request)
    {
        desa::create([
            'nama'=> $request->name,
            'id_kecamatan'=> $request->kecamatan
        ]);
        return response()->json([
            'success' => true,
            'message' => 'Desa Created'
        ]);
    }

    public function edit(Request $request, $id)
    {
        $data = desa::findOrFail($id);

        return $data;
    }

    public function update(Request $request, $id)
    {
        $data = desa::findOrFail($id);
        $data->update([
            'nama'=> $request->name,
            'id_kecamatan'=> $request->kecamatan
        ]);
        return response()->json([
            'success' => true,
            'message' => 'Desa Updated'
        ]);
    }

    public function destroy($id)
    {
        $data = desa::findOrFail($id);

        $data->destroy($id);

        return response()->json([
            'success'   => true,
            'message'   => 'Desa Deleted'
        ]);
    }
}
