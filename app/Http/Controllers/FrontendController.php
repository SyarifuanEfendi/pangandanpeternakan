<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\sip;
use App\Models\sipp;
use App\Models\nkv;

class FrontendController extends Controller
{
    public function index()
    {
        $data = DB::table('artikel')->orderby('id', 'desc')->get();
        return view('artikel', compact('data'));
    }
    public function alur()
    {
        return view('alur');
    }
    public function artikel_detail($id)
    {
        $data = DB::table('artikel')->where('slug', $id)->first();
        return view('artikel_detail', compact('data'));
    }
    public function search(Request $request)
    {
        if ($request->has('nik')) {
            $sip=sip::where('nik',$request->nik)->get();
            $sip1 = "";
            $sipp=sipp::where('nik',$request->nik)->get();
            $sipp1 = "";
            $nkv=nkv::where('nik',$request->nik)->get();
            $nkv1 = "";
            if ( count($sip)>0 ) {
                foreach ($sip as $datas => $data) {
                    $sip1 = $data->status;
                }
            }else {
                $sip1 = "Tidak Ada";
            }
            if ( count($sipp)>0 ) {
                foreach ($sipp as $datas => $data) {
                    $sipp1 = $data->status;
                }
            }else {
                $sipp1 = "Tidak Ada";
            }
            if ( count($nkv)>0 ) {
                foreach ($nkv as $datas => $data) {
                    $nkv1 = $data->status;
                }
            }else{
                $nkv1 = "Tidak Ada";
            }

            $datas = [
                array(
                    "name" => "SIP",
                    "keterangan" => $sip1
                ),
                array(
                    "name" => "SIPP",
                    "keterangan" => $sipp1
                ),
                array(
                    "name" => "NKV",
                    "keterangan" => $nkv1
                ),
            ];
            return $datas;
        }

        return response([]);
    }
}
