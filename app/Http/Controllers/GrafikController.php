<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\laporan;
use App\Models\kelompok;

class GrafikController extends Controller
{
    public function index()
    {
        $kelompok = kelompok::count();
        $laporan = laporan::groupBy('id_kelompok')->count();
        $kurang = $kelompok-$laporan;
        $dataPoints = [];
        $datas = [
            array(
                "name" => "Sudah Upload",
                "total_usage" => $laporan
            ),
            array(
                "name" => "Belum Upload",
                "total_usage" => $kurang
            )
        ];

        foreach ($datas as $data) {
            $dataPoints[] = [
                "name" => $data['name'],
                "y" => floatval($data['total_usage'])
            ];
        }
        return view("page.grafik.index", [
            "data" => json_encode($dataPoints)
        ]);
    }
}
