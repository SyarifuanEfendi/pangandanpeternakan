<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\kecamatan;

class KecamatanController extends Controller
{
    public function index()
    {
        // $data = kecamatan::all();
        return view('page.kecamatan.index');
    }

    public function apiKecamatan()
    {
        $data = kecamatan::all();

        return Datatables::of($data)
            ->addColumn('action', function($data){
                return
                '<a onclick="editForm('. $data->id .')" class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-edit"></i> Edit</a> ' .
                '<a onclick="deleteData('. $data->id .')" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i> Delete</a> ';
            })
            ->rawColumns(['action'])
            ->rawColumns(['action'])
            ->make(true);
    }

    public function show(Request $request)
    {
        # code...
    }

    public function create(Request $request)
    {
        # code...
    }

    public function store(Request $request)
    {
        kecamatan::create([
            'nama'      => $request['name'],
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Kecamatan Created'
        ]);
    }

    public function edit(Request $request, $id)
    {
        $data = kecamatan::findOrFail($id);

        return $data;
    }

    public function update(Request $request, $id)
    {
        $data = kecamatan::findOrFail($id);

        $data->update([
            'nama'      => $request['name'],
        ]);

        return response()->json([
            'success'   => true,
            'message'   => 'Kecamatan Updated'
        ]);
    }

    public function destroy($id)
    {
        $data = kecamatan::findOrFail($id);

        $data->destroy($id);

        return response()->json([
            'success'   => true,
            'message'   => 'Kecamatan Deleted'
        ]);
    }
}
