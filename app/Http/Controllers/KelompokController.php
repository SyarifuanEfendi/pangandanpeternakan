<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\kelompok;
use App\Models\desa;
use App\Models\kecamatan;
use App\Models\users;
use App\Models\bantuan;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

class KelompokController extends Controller
{
    public function index()
    {
        $data = kelompok::all();
        $desa = desa::all();
        $kecamatan = kecamatan::all();
        return view('page.kelompok.index', compact('data','desa','kecamatan'));
    }

    public function show(Request $request)
    {
        # code...
    }

    public function create(Request $request)
    {
        # code...
    }

    public function store(Request $request)
    {
        $kelompok = kelompok::create([
            'tanggal'      => Carbon::now()->toDateTimeString(),
            'nama_kelompok'=> $request['name'],
            'id_desa'      => $request['desa'],
            'id_kecamatan' => $request['kecamatan'],
            'nama_ketua'   => $request['ketua'],
            'telp'         => $request['telp'],
            'nik'          => $request['nik'],
            'bantuan'      => $request['bantuan'],
        ]);

        $kelompokId = DB::getPdo()->lastInsertId();;
        $username = strtolower(str_replace(' ', '', $request->name));

        for ($i=0; $i < count($request->item); $i++) {
            $cek = $request->item;
            $cek1 = $request->jumlah;
            bantuan::create([
                'nama' => $cek[$i],
                'jumlah' => $cek1[$i],
                'id_kelompok' => $kelompokId
            ]);
        }

        users::create([
            'username' => $username,
            'nama'=>$request->name,
            'role'=>'editor',
            'aplikasi'=>'1',
            'password'=> bcrypt('123456'),
            'id_kelompok'=>$kelompokId,
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Kecamatan Created'
        ]);
    }

    public function edit(Request $request, $id)
    {
        $data = kelompok::findOrFail($id);

        return $data;
    }

    public function update(Request $request, $id)
    {
        $data = kelompok::findOrFail($id);
        $data->update([
            'nama_kelompok'=> $request['name'],
            'id_desa'      => $request['desa'],
            'id_kecamatan' => $request['kecamatan'],
            'nama_ketua'   => $request['ketua'],
            'telp'         => $request['telp'],
            'nik'          => $request['nik'],
            'bantuan'      => $request['bantuan'],
        ]);
        $user = users::where('id_kelompok',$id)->first();
        $username = strtolower(str_replace(' ', '', $request->name));

        $user->update([
            'username' => $username,
            'nama'=>$request->name
        ]);
        return response()->json([
            'success' => true,
            'message' => 'Kelompok Updated'
        ]);
    }

    public function destroy($id)
    {
        $data = kelompok::findOrFail($id);

        $data->destroy($id);

        return response()->json([
            'success'   => true,
            'message'   => 'Kelompok Deleted'
        ]);
    }
}
