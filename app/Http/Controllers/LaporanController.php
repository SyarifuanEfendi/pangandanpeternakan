<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\laporan;
use App\Models\kelompok;
use App\Models\pemanfaatan;
use App\Models\hasilpanen;
use App\Models\bantuan;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;

class LaporanController extends Controller
{
    public function index()
    {
        $user_id = auth()->user()->id_kelompok;
        if (auth()->user()->role == 'admin') {
            $kelompok = kelompok::all();
            $data = laporan::all();
            $bantuan = bantuan::all();
        }else {
            $kelompok = kelompok::where('id', $user_id)->get();
            $data = laporan::where('id_kelompok', $user_id)->get();
            $bantuan = bantuan::where('id_kelompok', $user_id)->where('id_laporan',null)->get();
        }
        return view('page.laporan.index', compact('data','kelompok','bantuan'));
    }

    public function detail(Request $request, $id)
    {
        $user_id = auth()->user()->id_kelompok;
        $data = DB::select("SELECT l.tanggal, jumlah_anggota , pemanfaatan , hasil , nama_kelompok, nama_ketua, telp, nik, bantuan ,d.nama as desa, k2.nama as kecamatan  FROM laporan l
                    join kelompok k on l.id_kelompok = k.id
                    join desa d on k.id_desa = d.id
                    join kecamatan k2 on k.id_kecamatan = k2.id
                    where l.id = '$id'");
        $pemanfaat = pemanfaatan::where('id_laporan', $id)->get();
        $hasil = hasilpanen::where('id_laporan', $id)->get();
        $bantuan = bantuan::where('id_laporan', $id)->get();

        return view('page.laporan.detail', compact('data','pemanfaat','hasil','bantuan'));
    }

    public function show(Request $request)
    {
        # code...
    }

    public function create(Request $request)
    {
        # code...
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'imagemanfaatan' => 'required',
            // 'imagemanfaatan.*' => 'mimes:csv,txt,xlx,xls,pdf'
        ]);
        $laporan = new laporan();

        $laporan->id_kelompok = $request->kelompok;
        $laporan->tanggal = Carbon::now()->toDateTimeString();
        $laporan->jumlah_anggota = $request->jumlah;
        $laporan->pemanfaatan = $request->pemanfaatan;
        $laporan->hasil = $request->hasil;

        $laporan->save();

        //Getting Last inserted id

        $insertedId = $laporan->id;

        for ($i=0; $i < count($request->jumlahh); $i++) {
            $cek = $request->item;
            $cek1 = $request->jumlahh;
            $bantuan = DB::table('bantuan')
                ->where('id_kelompok', $request->kelompok)
                ->where('id_laporan',null)
                ->where('nama', $cek[$i]);
            $bantuan->update([
                'jumlah_manfaat' => $cek1[$i],
                'id_laporan' => $insertedId
            ]);
        }

        if($request->TotalFilesimage > 0)
        {

            for ($x = 0; $x < $request->TotalFilesimage; $x++)
            {

                if ($request->hasFile('imagemanfaatan'.$x))
                {
                    $file      = $request->file('imagemanfaatan'.$x);
                    $tujuan_upload = 'file/laporanpemanfaatan';

                    // $path = $file->store('public/file/laporanpemanfaatan');
                    // $name = time().$file->getClientOriginalName();
                    $name = time()."_".$file->getClientOriginalName();
                    $file->move($tujuan_upload,$name);

                    $insert[$x]['id_laporan'] = $insertedId;
                    $insert[$x]['foto'] = $name;
                }
            }

            pemanfaatan::insert($insert);

        }
        if($request->TotalFileshasil > 0)
        {

            for ($x = 0; $x < $request->TotalFileshasil; $x++)
            {

                if ($request->hasFile('imagehasil'.$x))
                {
                    $file      = $request->file('imagehasil'.$x);
                    $tujuan_upload = 'file/laporanhasil';

                    // $path = $file->store('public/file/laporanhasil');
                    // $name = time().$file->getClientOriginalName();
                    $name = time()."_".$file->getClientOriginalName();
                    $file->move($tujuan_upload,$name);

                    $insert[$x]['id_laporan'] = $insertedId;
                    $insert[$x]['foto'] = $name;
                }
            }

            hasilpanen::insert($insert);

        }
        else
        {
            return response()->json(["message" => "Please try again."]);
        }
        return response()->json([
            'success' => true,
            'message' => 'Upload Data '
        ]);
    }

    public function edit(Request $request, $id)
    {
        # code...
    }

    public function update(Request $request, $id)
    {
        # code...
    }

    public function destroy($id)
    {

        $cek1 = DB::select("select * from pemanfaatan where id_laporan = '$id'");
        foreach ($cek1 as $data) {
            File::delete(public_path("file/laporanpemanfaatan/".$data->foto));
        }
        $cek2 = DB::select("select * from hasilpanen where id_laporan = '$id'");
        foreach ($cek2 as $data) {
            File::delete(public_path("file/laporanhasil/".$data->foto));
        }
        $data = laporan::findOrFail($id);

        $data->destroy($id);

        DB::select("delete from pemanfaatan where id_laporan = '$id'");
        DB::select("delete from hasilpanen where id_laporan = '$id'");
        DB::select("update bantuan set jumlah_manfaat=null, id_laporan=null where id_laporan = '$id'");

        return response()->json([
            'success'   => true,
            'message'   => 'Laporan Deleted'
        ]);
    }
}
