<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\nkv;
use ZipArchive;

class NkvController extends Controller
{
    public function index()
    {
        $data = nkv::all();
        return view('page.nkv.index', compact('data'));
    }

    public function show(Request $request)
    {
        # code...
    }

    public function create(Request $request)
    {
        # code...
    }

    public function download($id)
    {
        $zip = new ZipArchive;

        $fileName = 'arsip_berkas.zip';

        if ($zip->open(public_path('file/zip/'.$fileName), ZipArchive::CREATE) === TRUE)
        {
            $cek = nkv::where('id', $id)->get();
            foreach ($cek as $b) {
                //parameter pertama berisi path/link dari berkas yang akan kita arsipkan kedalam zip sedangkan parameter kedua merupakan path/link/filename baru yang ada di dalam arsip zip
                $zip->addFile(public_path("file/nkv/".$b->ktp), $b->ktp);
                $zip->addFile(public_path("file/nkv/".$b->nib), $b->nib);
                $zip->addFile(public_path("file/nkv/".$b->siup), $b->siup);
                $zip->addFile(public_path("file/nkv/".$b->npwp), $b->npwp);
                $zip->addFile(public_path("file/nkv/".$b->domisili), $b->domisili);
                $zip->addFile(public_path("file/nkv/".$b->pernyataan), $b->pernyataan);
                $zip->addFile(public_path("file/nkv/".$b->bukti_perjanjian), $b->bukti_perjanjian);
                $zip->addFile(public_path("file/nkv/".$b->kuasa), $b->kuasa);
            }

            $zip->close();
            return response()->download(public_path('file/zip/'.$fileName));
        }
   }

   public function proses($id)
   {
       $cek = nkv::where('id', $id)->get();
       $data = nkv::findOrFail($id);

       if ($cek[0]->status == "proses") {
            $data->update([
                'status'   => "belum di ambil",
            ]);
       }elseif ($cek[0]->status == "belum di ambil") {
            $data->update([
                'status'   => "selesai",
            ]);
       }else {
            return response()->json([
                'success'   => true,
                'message'   => 'Nomor Kontrol Veteriner Selesai'
            ]);
       }

       return response()->json([
           'success'   => true,
           'message'   => 'Nomor Kontrol Veteriner Updated'
       ]);

   }

    public function store(Request $request)
    {
        $validator = $request->validate([
            'ktp' => 'required|mimes:pdf',
            'nib' => 'required|mimes:pdf',
            'siup' => 'required|mimes:pdf',
            'npwp' => 'required|mimes:pdf',
            'keterangan' => 'required|mimes:pdf',
            'pernyataan' => 'required|mimes:pdf',
            'perjanjian' => 'required|mimes:pdf',
            'kuasa' => 'required|mimes:pdf'
        ]);

        $nama_file = '';
        $nama_file1 = '';
        $nama_file2 = '';
        $nama_file3 = '';
        $nama_file4 = '';
        $nama_file5 = '';
        $nama_file6 = '';
        $nama_file7 = '';

        $tujuan_upload = 'file/nkv';

        $file = $request->file('ktp');
        $nama_file = time()."_ktp_".$file->getClientOriginalName();
        $file->move($tujuan_upload,$nama_file);

        $file1 = $request->file('nib');
        $nama_file1 = time()."_nib_".$file1->getClientOriginalName();
        $file1->move($tujuan_upload,$nama_file1);

        $file2 = $request->file('siup');
        $nama_file2 = time()."_siup_".$file2->getClientOriginalName();
        $file2->move($tujuan_upload,$nama_file2);

        $file3 = $request->file('npwp');
        $nama_file3 = time()."_npwp_".$file3->getClientOriginalName();
        $file3->move($tujuan_upload,$nama_file3);

        $file4 = $request->file('keterangan');
        $nama_file4 = time()."_keterangan_".$file4->getClientOriginalName();
        $file4->move($tujuan_upload,$nama_file4);

        $file5 = $request->file('pernyataan');
        $nama_file5 = time()."_pernyataan_".$file5->getClientOriginalName();
        $file5->move($tujuan_upload,$nama_file5);

        $file6 = $request->file('perjanjian');
        $nama_file6 = time()."_perjanjian_".$file6->getClientOriginalName();
        $file6->move($tujuan_upload,$nama_file6);

        $file7 = $request->file('kuasa');
        $nama_file7 = time()."_kuasa_".$file7->getClientOriginalName();
        $file7->move($tujuan_upload,$nama_file7);

        nkv::create([
            'ktp' => $nama_file,
            'nik' => $request->nik,
            'nib'=> $nama_file1,
            'siup' => $nama_file2,
            'npwp' => $nama_file3,
            'domisili' => $nama_file4,
            'pernyataan' => $nama_file5,
            'bukti_perjanjian' => $nama_file6,
            'kuasa' => $nama_file7
        ]);


        return response()->json([
            'success' => true,
            'message' => 'Upload Data Nomor Kontrol Veteriner'
        ]);
    }

    public function edit(Request $request, $id)
    {
        # code...
    }

    public function update(Request $request, $id)
    {
        # code...
    }

    public function destroy($id)
    {
        # code...
    }
}
