<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\sip;


class SipController extends Controller
{
    public function index()
    {
        $data = sip::all();
        return view('page.sip.index', compact('data'));
    }

    public function show(Request $request)
    {
        # code...
    }

    public function create(Request $request)
    {
        # code...
    }

    public function download($id)
    {
        $zip = new ZipArchive;

        $fileName = 'arsip_berkas.zip';

        if ($zip->open(public_path('file/zip/'.$fileName), ZipArchive::CREATE) === TRUE)
        {
            $cek = sip::where('id', $id)->get();
            foreach ($cek as $b) {
                //parameter pertama berisi path/link dari berkas yang akan kita arsipkan kedalam zip sedangkan parameter kedua merupakan path/link/filename baru yang ada di dalam arsip zip
                $zip->addFile(public_path("file/sip/".$b->ktp), $b->ktp);
                $zip->addFile(public_path("file/sip/".$b->npwp), $b->npwp);
                $zip->addFile(public_path("file/sip/".$b->foto), $b->foto);
                $zip->addFile(public_path("file/sip/".$b->ijazah), $b->ijazah);
                $zip->addFile(public_path("file/sip/".$b->sertifikat), $b->sertifikat);
                $zip->addFile(public_path("file/sip/".$b->rekom), $b->rekom);
                $zip->addFile(public_path("file/sip/".$b->memperdagangkan), $b->memperdagangkan);
                $zip->addFile(public_path("file/sip/".$b->kesejahteraan), $b->kesejahteraan);
                $zip->addFile(public_path("file/sip/".$b->tempat), $b->tempat);
                $zip->addFile(public_path("file/sip/".$b->permohonan), $b->permohonan);
            }

            $zip->close();
            return response()->download(public_path('file/zip/'.$fileName));
        }
   }

   public function proses($id)
   {
       $cek = sip::where('id', $id)->get();
       $data = sip::findOrFail($id);

       if ($cek[0]->status == "proses") {
            $data->update([
                'status'   => "belum di ambil",
            ]);
       }elseif ($cek[0]->status == "belum di ambil") {
            $data->update([
                'status'   => "selesai",
            ]);
       }else {
            return response()->json([
                'success'   => true,
                'message'   => 'Surat Izin Praktik Selesai'
            ]);
       }

       return response()->json([
           'success'   => true,
           'message'   => 'Surat Izin Praktik Updated'
       ]);

   }

    public function store(Request $request)
    {
        $validator = $request->validate([
            'ktp' => 'required|mimes:pdf',
            'foto' => 'required|mimes:pdf',
            'ijazah' => 'required|mimes:pdf',
            'kompetensi' => 'required|mimes:pdf',
            'rekomendasi' => 'required|mimes:pdf',
            'pernyataan' => 'required|mimes:pdf',
            'kesejahteraan' => 'required|mimes:pdf',
            'keterangan' => 'required|mimes:pdf',
            'sip' => 'required|mimes:pdf'
        ]);
        $nama_file = '';
        $nama_file1 = '';
        $nama_file2 = '';
        $nama_file3 = '';
        $nama_file4 = '';
        $nama_file5 = '';
        $nama_file6 = '';
        $nama_file7 = '';
        $nama_file8 = '';

        // menyimpan data file yang diupload ke variabel $file
        $file = $request->file('ktp');
        $file1 = $request->file('foto');
        $file2 = $request->file('ijazah');
        $file3 = $request->file('kompetensi');
        $file4 = $request->file('rekomendasi');
        $file5 = $request->file('pernyataan');
        $file6 = $request->file('kesejahteraan');
        $file7 = $request->file('keterangan');
        $file8 = $request->file('sip');

        $nama_file = time()."_ktp_".$file->getClientOriginalName();
        $nama_file1 = time()."_foto_".$file1->getClientOriginalName();
        $nama_file2 = time()."_ijazah_".$file2->getClientOriginalName();
        $nama_file3 = time()."_kompetensi_".$file3->getClientOriginalName();
        $nama_file4 = time()."_rekomendasi_".$file4->getClientOriginalName();
        $nama_file5 = time()."_pernyataan_".$file5->getClientOriginalName();
        $nama_file6 = time()."_kesejahteraan_".$file6->getClientOriginalName();
        $nama_file7 = time()."_keterangan_".$file7->getClientOriginalName();
        $nama_file8 = time()."_sip_".$file8->getClientOriginalName();

            // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = 'file/sip';
        $file->move($tujuan_upload,$nama_file);
        $file1->move($tujuan_upload,$nama_file1);
        $file2->move($tujuan_upload,$nama_file2);
        $file3->move($tujuan_upload,$nama_file3);
        $file4->move($tujuan_upload,$nama_file4);
        $file5->move($tujuan_upload,$nama_file5);
        $file6->move($tujuan_upload,$nama_file6);
        $file7->move($tujuan_upload,$nama_file7);
        $file8->move($tujuan_upload,$nama_file8);


        sip::create([
            'ktp' => $nama_file,
            'nik' => $request->nik,
            'foto'=> $nama_file1,
            'ijazah' => $nama_file2,
            'sertifikat' => $nama_file3,
            'rekom' => $nama_file4,
            'memperdagangkan' => $nama_file5,
            'kesejahteraan' => $nama_file6,
            'tempat' => $nama_file7,
            'permohonan' => $nama_file8
        ]);


        return response()->json([
            'success' => true,
            'message' => 'Upload Data Surat Izin Praktik Dokter Hewan'
        ]);
    }

    public function edit(Request $request, $id)
    {
        # code...
    }

    public function update(Request $request, $id)
    {
        # code...
    }

    public function destroy($id)
    {
        # code...
    }
}
