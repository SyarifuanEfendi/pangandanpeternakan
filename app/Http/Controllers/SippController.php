<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\sipp;
use ZipArchive;

class SippController extends Controller
{
    public function index()
    {
        $data = sipp::all();
        return view('page.sipp.index', compact('data'));
    }

    public function show(Request $request)
    {
        # code...
    }

    public function create(Request $request)
    {
        # code...
    }
    public function download($id)
    {
        $zip = new ZipArchive;

        $fileName = 'arsip_berkas.zip';

        if ($zip->open(public_path('file/zip/'.$fileName), ZipArchive::CREATE) === TRUE)
        {
            $cek = sip::where('id', $id)->get();
            foreach ($cek as $b) {
                //parameter pertama berisi path/link dari berkas yang akan kita arsipkan kedalam zip sedangkan parameter kedua merupakan path/link/filename baru yang ada di dalam arsip zip
                $zip->addFile(public_path("file/sipp/".$b->ktp), $b->ktp);
                $zip->addFile(public_path("file/sipp/".$b->npwp), $b->npwp);
                $zip->addFile(public_path("file/sipp/".$b->foto), $b->foto);
                $zip->addFile(public_path("file/sipp/".$b->ijazah), $b->ijazah);
                $zip->addFile(public_path("file/sipp/".$b->perjanjian), $b->perjanjian);
                $zip->addFile(public_path("file/sipp/".$b->rekom), $b->rekom);
                $zip->addFile(public_path("file/sipp/".$b->kompetensi), $b->kompetensi);
                $zip->addFile(public_path("file/sipp/".$b->tempat), $b->tempat);
                $zip->addFile(public_path("file/sipp/".$b->permohonan), $b->permohonan);
            }

            $zip->close();
            return response()->download(public_path('file/zip/'.$fileName));
        }
   }

   public function proses($id)
   {
       $cek = sipp::where('id', $id)->get();
       $data = sipp::findOrFail($id);

       if ($cek[0]->status == "proses") {
            $data->update([
                'status'   => "belum di ambil",
            ]);
       }elseif ($cek[0]->status == "belum di ambil") {
            $data->update([
                'status'   => "selesai",
            ]);
       }else {
            return response()->json([
                'success'   => true,
                'message'   => 'Surat Izin Praktik Paramedik Selesai'
            ]);
       }

       return response()->json([
           'success'   => true,
           'message'   => 'Surat Izin Praktik Paramedik Updated'
       ]);

   }

    public function store(Request $request)
    {
        $validator = $request->validate([
            'ktp' => 'required|mimes:pdf',
            'foto' => 'required|mimes:pdf',
            'npwp' => 'required|mimes:pdf',
            'ijazah' => 'required|mimes:pdf',
            'perjanjian' => 'required|mimes:pdf',
            'rekomendasi' => 'required|mimes:pdf',
            'kompetensi' => 'required|mimes:pdf',
            'keterangan' => 'required|mimes:pdf',
            'sipp' => 'required|mimes:pdf'
        ]);

        $nama_file = '';
        $nama_file1 = '';
        $nama_file2 = '';
        $nama_file3 = '';
        $nama_file4 = '';
        $nama_file5 = '';
        $nama_file6 = '';
        $nama_file7 = '';
        $nama_file8 = '';

        $tujuan_upload = 'file/sipp';

        $file = $request->file('ktp');
        $nama_file = time()."_ktp_".$file->getClientOriginalName();
        $file->move($tujuan_upload,$nama_file);

        $file1 = $request->file('foto');
        $nama_file1 = time()."_foto_".$file1->getClientOriginalName();
        $file1->move($tujuan_upload,$nama_file1);

        $file2 = $request->file('npwp');
        $nama_file2 = time()."_npwp_".$file2->getClientOriginalName();
        $file2->move($tujuan_upload,$nama_file2);

        $file3 = $request->file('ijazah');
        $nama_file3 = time()."_ijazah_".$file3->getClientOriginalName();
        $file3->move($tujuan_upload,$nama_file3);

        $file4 = $request->file('perjanjian');
        $nama_file4 = time()."_perjanjian_".$file4->getClientOriginalName();
        $file4->move($tujuan_upload,$nama_file4);

        $file5 = $request->file('rekomendasi');
        $nama_file5 = time()."_rekomendasi_".$file5->getClientOriginalName();
        $file5->move($tujuan_upload,$nama_file5);

        $file6 = $request->file('kompetensi');
        $nama_file6 = time()."_kompetensi_".$file6->getClientOriginalName();
        $file6->move($tujuan_upload,$nama_file6);

        $file7 = $request->file('keterangan');
        $nama_file7 = time()."_keterangan_".$file7->getClientOriginalName();
        $file7->move($tujuan_upload,$nama_file7);

        $file8 = $request->file('sipp');
        $nama_file8 = time()."_sipp_".$file8->getClientOriginalName();
        $file8->move($tujuan_upload,$nama_file8);

        sipp::create([
            'ktp' => $nama_file,
            'nik' => $request->nik,
            'foto'=> $nama_file1,
            'npwp' => $nama_file2,
            'ijazah' => $nama_file3,
            'perjanjian' => $nama_file4,
            'rekomendasi' => $nama_file5,
            'kompetensi' => $nama_file6,
            'tempat' => $nama_file7,
            'permohonan' => $nama_file8
        ]);


        return response()->json([
            'success' => true,
            'message' => 'Surat Izin Praktik Paramedik'
        ]);
    }

    public function edit(Request $request, $id)
    {
        # code...
    }

    public function update(Request $request, $id)
    {
        # code...
    }

    public function destroy($id)
    {
        # code...
    }
}
