<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\users;
use App\Models\kelompok;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    public function index()
    {
        $id = auth()->user()->aplikasi;
        $data=users::join('kelompok','users.id_kelompok','=','kelompok.id')
            ->where('aplikasi', $id)
            ->select('users.*','kelompok.nama_kelompok')
            ->get();
        $kelompok=kelompok::all();
        return view('page.user.index',compact('data','kelompok'));
    }

    public function show(Request $request)
    {
        # code...
    }

    public function create(Request $request)
    {
        # code...
    }

    public function store(Request $request)
    {
        $cek = users::where('username', $request->username)->first();
        if ($cek != null) {
            return response()->json([
                'success' => false,
                'message' => 'Username Sudah Ada'
            ]);
        }
        if ($request->kelompok != null) {
            users::create([
                'username' => $request->username,
                'nama'=> $request->name,
                'role'=> $request->role,
                'aplikasi'=>'1',
                'password'=> bcrypt('123456'),
                'id_kelompok'=>$request->kelompok,
            ]);
        }else {
            users::create([
                'username' => $request->username,
                'nama'=> $request->name,
                'role'=> 'admin',
                'aplikasi'=>'2',
                'password'=> bcrypt('123456'),
                'id_kelompok'=> 0,
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => 'User Created'
        ]);
    }

    public function edit(Request $request, $id)
    {
        $data = users::findOrFail($id);

        return $data;
    }

    public function update(Request $request, $id)
    {
        // $cek = users::where('username', $request->username)->first();
        // if ($cek != null) {
        //     return response()->json([
        //         'success' => false,
        //         'message' => 'Username Sudah Ada'
        //     ]);
        // }
        $data = users::findOrFail($id);
        if ($request->kelompok != null) {
            $data->update([
                'username' => $request->username,
                'nama'=> $request->name,
                'role'=> $request->role,
                'id_kelompok'=>$request->kelompok,
            ]);
        }else {
            $data->update([
                'username' => $request->username,
                'nama'=> $request->name
            ]);
        }

        return response()->json([
            'success'   => true,
            'message'   => 'Users Updated'
        ]);
    }

    public function destroy($id)
    {
        $data = users::findOrFail($id);

        $data->destroy($id);

        return response()->json([
            'success'   => true,
            'message'   => 'Users Deleted'
        ]);
    }
}
