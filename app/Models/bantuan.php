<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class bantuan extends Model
{
    protected $table = 'bantuan';
    protected $fillable = ['nama','id_kelompok','jumlah','jumlah_manfaat','id_laporan'];
    public $timestamps = true;

    use HasFactory;
}
