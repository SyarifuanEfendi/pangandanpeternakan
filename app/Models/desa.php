<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class desa extends Model
{
    protected $table = 'desa';
    protected $fillable = ['id_kecamatan','nama'];
    public $timestamps = true;

    use HasFactory;
}
