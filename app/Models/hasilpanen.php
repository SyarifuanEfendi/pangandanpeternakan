<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class hasilpanen extends Model
{
    protected $table = 'hasilpanen';
    protected $fillable = ['id_laporan','foto'];
    public $timestamps = true;
    use HasFactory;
}
