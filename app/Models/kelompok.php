<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class kelompok extends Model
{
    protected $table = 'kelompok';
    protected $fillable = ['id','tanggal','nama_kelompok','id_desa','id_kecamatan','nama_ketua','telp','nik','bantuan'];
    public $timestamps = true;
    use HasFactory;
}
