<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class laporan extends Model
{
    protected $table = 'laporan';
    protected $fillable = ['id_kelompok','tanggal','jumlah_anggota','pemanfaatan','hasil','id_pemanfaatan','id_hasil'];
    public $timestamps = true;
    use HasFactory;
}
