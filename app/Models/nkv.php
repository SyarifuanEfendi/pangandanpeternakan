<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class nkv extends Model
{
    protected $table = 'nkv';
    protected $fillable = ['nik','ktp','nib','siup','npwp','domisili','pernyataan','bukti_perjanjian','kuasa','status'];
    public $timestamps = true;
    use HasFactory;
}
