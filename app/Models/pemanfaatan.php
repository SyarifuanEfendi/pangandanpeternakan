<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pemanfaatan extends Model
{
    protected $table = 'pemanfaatan';
    protected $fillable = ['id_laporan','foto'];
    public $timestamps = true;
    use HasFactory;
}
