<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class sip extends Model
{
    protected $table = 'sip';
    protected $fillable = ['nik','ktp','npwp','foto','ijazah','sertifikat','rekom','memperdagangkan','kesejahteraan','tempat','permohonan','status'];
    public $timestamps = true;
    use HasFactory;
}
