<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class sipp extends Model
{
    protected $table = 'sipp';
    protected $fillable = ['nik','ktp','npwp','foto','ijazah','perjanjian','rekom','kompetensi','tempat','permohonan','status'];
    public $timestamps = true;
    use HasFactory;
}
