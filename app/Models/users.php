<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class users extends Model
{
    protected $table = 'users';
    protected $fillable = ['nama','username','password','role','aplikasi','id_kelompok'];
    public $timestamps = true;
    use HasFactory;
}
