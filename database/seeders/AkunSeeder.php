<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\users;

class AkunSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'username' => 'admin',
                'nama'=>'ini akun Admin',
                'role'=>'admin',
                'aplikasi'=>'1',
                'password'=> bcrypt('123456'),
                'id_kelompok'=>'1',
            ],
            [
                'username' => 'user',
                'nama'=>'ini akun User (non admin)',
                'role'=>'editor',
                'aplikasi'=>'1',
                'password'=> bcrypt('123456'),
                'id_kelompok'=>'1',
            ],
        ];

        foreach ($user as $key => $value) {
            users::create($value);
        }
    }
}
