@extends('index')

@section('title')
    Alur | Izin Praktek
@endsection

@section('customcss')

@endsection


@section('content')

<div id="page-wrapper" style="margin:0 0 0 0; background-image: url('bg.png'); min-height: 469px;">
            <div class="container-fluid">
                <!-- .row -->
                <div class="row">
                    <div class="col-md-12">
                        <div style="margin-bottom: 30px">
                            <h3 class="text-center">
                                <b>
                                <i class="mdi mdi-layers mdi-36px mdi-spin"></i>Alur Permohonan Izin Praktek Dokter Hewan Kabupaten Kediri
                                </b>
                            </h3>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div style="margin-bottom: 30px">
                            <h4 class="text-center">
                                <center>
                                    <label for="">Cek Berkas : </label>
                                <!-- <form class="form-inline ml-4" style="width: 100%;" action="#" method="post">
                                        <input style="width: 25%;" type="text" name="a" class="form-control" type="search" placeholder="Masukkan NIK" aria-label="Search" required>
                                            <button class="btn bg-primary" type="submit">
                                                Cari
                                            </button>
                                            <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target-nik="1" data-target="#myModal">Cari</button>
                                </form> -->
                                <form class="form-inline mt-4 mb-4" action="javascript:searchh();">
                                    <input class="form-control mr-sm-2" type="search" placeholder="Masukkan NIK" aria-label="Search" id="search-inputt">
                                    <button class="btn bg-primary" data-toggle="modal" data-target="#hasill" type="submit">
                                    Search
                                    </button>
                                </form>
                                </center>
                            </h4>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!-- .row -->
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="font-size: 18px"><i class="ti-settings"></i> Surat Izin Praktik Dokter Hewan <span class="label label-info m-l-5">New</span>
                                <div class="panel-action"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a> <a href="#" data-perform="panel-dismiss"><i class="ti-close"></i></a></div>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <table class="table table-hover">
                                        <tbody>
                                            <tr>
                                                <td><span class="badge badge-info">01</span></td>
                                                <td><span>KTP</span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="badge badge-info">02</span></td>
                                                <td><span>NPWP</span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="badge badge-info">03</span></td>
                                                <td><span>Pas Foto Berwarna Ukuran 4x6 (BACKGROUND PRIA MERAH, WANITA BIRU)</span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="badge badge-info">04</span></td>
                                                <td><span>Ijazah</span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="badge badge-info">05</span></td>
                                                <td><span>SERTIFIKAT KOMPETENSI Drh YANG DITERBITKAN ORGANISASI PROFESI</span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="badge badge-info">06</span></td>
                                                <td><span>SURAT REKOMENDASI DARI PDHI CABANG SETEMPAT</span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="badge badge-info">07</span></td>
                                                <td><span>PERNYATAAN MENGGUNAKAN ATAU MEMPERDAGANGKAN OBAT HEWAN YANG MEMILIKI NOMOR PENDAFTARAN SESUAI DG KETENTUAN PERATURAN PERUNDANG-UNDANGAN</span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="badge badge-info">08</span></td>
                                                <td><span>PERNYATAAN MEMENUHI PERSYARATAN KESEJAHTERAAN HEWAN</span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="badge badge-info">09</span></td>
                                                <td><span>SURAT KETERANGAN PERSYARATAN TEMPAT PELAYANAN</span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="badge badge-info">10</span></td>
                                                <td><span>PERMOHONAN REKOMENDASI SIP</span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <center>
                                        <button type="button" class="btn btn-success m-t-10" data-toggle="modal" onclick="addForm()">Upload Data</button>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="font-size: 18px"> Surat Izin Praktik Paramedik <span class="label label-info m-l-5">New</span>
                                <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a> <a href="#" data-perform="panel-dismiss"><i class="ti-close"></i></a> </div>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <table class="table table-hover">
                                        <tbody>
                                            <tr>
                                                <td><span class="badge badge-info">01</span></td>
                                                <td><span>KTP</span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="badge badge-info">02</span></td>
                                                <td><span>NPWP</span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="badge badge-info">03</span></td>
                                                <td><span>PAS FOTO BERWARNA UKURAN 4X6 2 LEMBAR (BACKGROUND PRIA MERAH, WANITA BIRU)</span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="badge badge-info">04</span></td>
                                                <td><span>IJAZAH SKH, D3 ATAU SEKOLAH KEJURUAN DIBIDANG KESEHATAN HEWAN</span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="badge badge-info">05</span></td>
                                                <td><span>PERJANJIAN KERJASAMA PENYELIAAN DOKTER HEWAN</span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="badge badge-info">06</span></td>
                                                <td><span>SURAT REKOMENDASI DARI ORGANISASI PROFESI PARAMEDIK VETERINER SETEMPAT</span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="badge badge-info">07</span></td>
                                                <td><span>SERTIFIKAT KOMPETENSI YANG DITERBITKAN OLEH LEMBAGA SERTIFIKAT PROFESI</span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="badge badge-info">08</span></td>
                                                <td><span>SURAT KETERANGAN PERSYARATAN TEMPAT PELAYANAN</span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="badge badge-info">09</span></td>
                                                <td><span>PERMOHONAN REKOMENDASI SIPP</span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <center>
                                        <button type="button" class="btn btn-success m-t-10" data-toggle="modal" data-target="#paramedik">Upload Data</button>
                                    </center>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="font-size: 18px"> Nomor Kontrol Veteriner
                                <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a> <a href="#" data-perform="panel-dismiss"><i class="ti-close"></i></a> </div>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <table class="table table-hover">
                                        <tbody>
                                            <tr>
                                                <td><span class="badge badge-info">01</span></td>
                                                <td><span>KTP</span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="badge badge-info">02</span></td>
                                                <td><span>NIB</span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="badge badge-info">03</span></td>
                                                <td><span>SIUP/TDP</span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="badge badge-info">04</span></td>
                                                <td><span>NPWP</span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="badge badge-info">05</span></td>
                                                <td><span>SURAT KETERANGAN DOMISILI UNIT USAHA YG DIKELUARKAN PEJABAT BERWENANG</span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="badge badge-info">06</span></td>
                                                <td><span>SURAT PERNYATAAN BERMATERAI YG MENERANGKAN BAHWA DOKUMEN YG DISAMPAIKAN BENAR DAN SAH</span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="badge badge-info">07</span></td>
                                                <td><span>BUKTI PERJANJIAN PENGELOLAAN USAHA BAGI PELAKU USAHA YG MELAKUKAN KEGIATAN DI TEMPAT USAHA MILIK ORANG LAIN</span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="badge badge-info">08</span></td>
                                                <td><span>SURAT KUASA BERMATERAI (BILA DIWAKILKAN OLEH PIHAK LAIN)</span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <center>
                                        <button type="button" class="btn btn-success m-t-10" data-toggle="modal" data-target="#kontrol">Upload Data</button>
                                    </center>
                                    <br><br><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>

@endsection
