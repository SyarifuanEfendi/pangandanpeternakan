@extends('index')

@section('title')
    Artikel | Izin Praktek
@endsection

@section('content')

<div class="container">
    <div id="blog" class="row">
        @foreach($data as $datas)
            <div class="col-md-12 blogShort">
                <h1>{{ $datas->judul }}</h1>
                <img src="{{ asset('file/artikel/'.$datas->gambar) }}" alt="post img" width="150" class="pull-left img-responsive thumb margin10 img-thumbnail">

                    <em>This snippet use <a href="" target="_blank">Sexy Sidebar Navigation</a></em>
                <article><p>
                {!! Str::limit( strip_tags( $datas->isi ), 200 ) !!}
                    </p></article>
                <a class="btn btn-blog pull-right marginBottom10" href="{{ url('artikel_detail/'.$datas->slug) }}">READ MORE</a>
            </div>
        @endforeach

        <div class="col-md-12 gap10"></div>
    </div>
</div>

@endsection
