<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('logo.png') }}">
    <title>@yield('title')</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('ampleadmin-minimal/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('ampleadmin-minimal/css/colors/default.css') }}" id="theme" rel="stylesheet">
    <script src="{{ asset('assets/sweetalert2/sweetalert2.min.js') }}"></script>
    <link href="{{ asset('assets/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet">
    <style>
        .blogShort{ border-bottom:1px solid #ddd;}
.add{background: #333; padding: 10%; height: 300px;}

.nav-sidebar {
    width: 100%;
    padding: 8px 0;
    border-right: 1px solid #ddd;
}
.nav-sidebar a {
    color: #333;
    -webkit-transition: all 0.08s linear;
    -moz-transition: all 0.08s linear;
    -o-transition: all 0.08s linear;
    transition: all 0.08s linear;
}
.nav-sidebar .active a {
    cursor: default;
    background-color: #34ca78;
    color: #fff;
}
.nav-sidebar .active a:hover {
    background-color: #37D980;
}
.nav-sidebar .text-overflow a,
.nav-sidebar .text-overflow .media-body {
    white-space: nowrap;
    overflow: hidden;
    -o-text-overflow: ellipsis;
    text-overflow: ellipsis;
}

.btn-blog {
    color: #ffffff;
    background-color: #37d980;
    border-color: #37d980;
    border-radius:0;
    margin-bottom:10px
}
.btn-blog:hover,
.btn-blog:focus,
.btn-blog:active,
.btn-blog.active,
.open .dropdown-toggle.btn-blog {
    color: white;
    background-color:#34ca78;
    border-color: #34ca78;
}
 h2{color:#34ca78;}
 .margin10{margin-bottom:10px; margin-right:10px;}
        #footer {
            height: 50px;
            background-color: #f5f5f5;
        }
        .container {
            width: auto;
            max-width: 680px;
            padding: 0 15px;
        }
        .container .credit {
            margin: 20px 0;
        }
    </style>
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <!-- <nav class="navbar navbar-default navbar-fixed-bottom">
            <div class="navbar-header">
                <a class="navbar-brand" style="color: #ffffff" href="#">WebSiteName</a>
                <ul class="navbar-nav">
                    <li class="active"><a href="#">Artikel</a></li>
                    <li class="waves-effect waves-light"><a href="#">Permohonan Izin</a></li>
                    <li>
                        <form class="app-search hidden-sm hidden-xs m-r-12" action="#" method="post">
                            <input type="text" name="a" class="form-control" type="search" placeholder="Masukkan NIK" aria-label="Search" required>
                                <button class="btn btn-success btn-rounded" type="submit" name="submit1">
                                    <i class="mdi mdi-magnify-plus-outline mdi-light"></i>
                                </button>
                        </form>
                    </li>
                </ul>
            </div>
        </nav> -->
<nav class="navbar navbar-default">
    <div class="container-fluid">
		<!-- Brand/logo -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#example-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar" style="background-color: #ffffff"></span>
				<span class="icon-bar" style="background-color: #ffffff"></span>
				<span class="icon-bar" style="background-color: #ffffff"></span>
			</button>
            <div>
                <img style="background: white; float: left; height: 50px; padding-right: 20px; font-size: 18px; line-height: 20px;" src="{{ asset('logo1.png') }}" />
            </div>
		</div>
		<!-- Collapsible Navbar -->
		<div class="collapse navbar-collapse" id="example-1">
			<ul class="nav navbar-nav">
				<li class="{{ Request::is('/') || Request::is('artikel_list') || Request::is('artikel_detail/*') ? 'active' : '' }}"><a href="{{url('artikel_list')}}">Artikel</a></li>
				<li class="{{ Request::is('alur') ? 'active' : '' }}"><a href="{{url('alur')}}">Alur Permohonan</a></li>


			</ul>
            <form class="navbar-form navbar-right mt-4 mb-4" action="javascript:search();">
                <label for="">Cek Berkas </label>
                    <!-- <div class="form-group">
                        <input type="text" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-default">Search</button> -->
                    <input class="form-control mr-sm-2" type="search" placeholder="Masukkan NIK" aria-label="Search" id="search-input">
                    <button class="btn bg-primary" data-toggle="modal" data-target="#hasil" type="submit">
                    Search
                    </button>
            </form>
		</div>
    </div>
</nav>
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->

        @yield('content')

        <div id="footer">
            <div class="container">
                <p class="text-muted credit"> <a href="#"> 2022 &copy; by SyarifuanEfendi</a>.</p>
            </div>
        </div>

        <!-- <footer class="footer text-center" style="position: sticky;"> 2022 &copy; by SyarifuanEfendi </footer> -->
        <!-- /#page-wrapper -->
    </div>
    <div class="modal fade" id="hasil" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Hasil Percarian</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="results"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="hasill" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Hasil Percarian</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="resultss"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="praktek" tabindex="-1" aria-labelledby="praktek" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Upload Data Surat Izin Praktik Dokter Hewan</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <!--FORM TAMBAH BARANG-->
                    <form method="POST" data-toggle="validator" enctype="multipart/form-data">
                    {{ csrf_field() }} {{ method_field('POST') }}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="">NIK</label>
                            <input type="text" class="form-control" id="nik" name="nik" required>
                        </div>
                        <div class="form-group">
                            <label for="">KTP</label>
                            <input type="file" class="form-control" id="ktp" name="ktp">
                            <span class="text-danger">{{ $errors->first('ktp') }}</span>
                        </div>
                        <div class="form-group">
                            <label for="">Pas Foto</label>
                            <input type="file" class="form-control" id="foto" name="foto">
                            @if($errors->has('foto'))
                                <small class="error">{{ $errors->first('foto') }}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="">Ijazah</label>
                            <input type="file" class="form-control" id="ijazah" name="ijazah">
                            @if($errors->has('ijazah'))
                                <small class="error">{{ $errors->first('ijazah') }}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="">Kompetensi yang Diterbitkan oleh Organisasi</label>
                            <input type="file" class="form-control" id="kompetensi" name="kompetensi">
                            @if($errors->has('kompetensi'))
                                <small class="error">{{ $errors->first('kompetensi') }}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="">Rekomendasi dari PDHI</label>
                            <input type="file" class="form-control" id="rekomendasi" name="rekomendasi">
                            @if($errors->has('rekomendasi'))
                                <small class="error">{{ $errors->first('rekomendasi') }}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="">Pernyataan Menggunakan/Memperdagangkan Obat Hewan</label>
                            <input type="file" class="form-control" id="pernyataan" name="pernyataan">
                            @if($errors->has('pernyataan'))
                                <small class="error">{{ $errors->first('pernyataan') }}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="">Pernyataan Memenuhi Persyaratan Kesejahteraan Hewan</label>
                            <input type="file" class="form-control" id="kesejahteraan" name="kesejahteraan">
                            @if($errors->has('kesejahteraan'))
                                <small class="error">{{ $errors->first('kesejahteraan') }}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="">Keterangan Persyaratan Tempat</label>
                            <input type="file" class="form-control" id="keterangan" name="keterangan">
                            @if($errors->has('keterangan'))
                                <small class="error">{{ $errors->first('keterangan') }}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="">Rekomendasi SIP</label>
                            <input type="file" class="form-control" id="sip" name="sip">
                            @if($errors->has('sip'))
                                <small class="error">{{ $errors->first('sip') }}</small>
                            @endif
                        </div>
                    <button type="submit" class="btn btn-primary">Simpan Data</button>
                    </form>
                <!--END FORM TAMBAH BARANG-->
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="paramedik" tabindex="-1" aria-labelledby="paramedik" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Upload Data Surat Izin Praktik Paramedik</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <!--FORM TAMBAH BARANG-->
                <form method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }} {{ method_field('POST') }}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="">NIK</label>
                            <input type="text" class="form-control" id="nik" name="nik" required>
                        </div>
                        <div class="form-group">
                            <label for="">KTP</label>
                            <input type="file" class="form-control" id="ktp" name="ktp" required>
                        </div>
                        <div class="form-group">
                            <label for="">NPWP</label>
                            <input type="file" class="form-control" id="npwp" name="npwp" required>
                        </div>
                        <div class="form-group">
                            <label for="">Pas Foto</label>
                            <input type="file" class="form-control" id="foto" name="foto" required>
                        </div>
                        <div class="form-group">
                            <label for="">Ijazah</label>
                            <input type="file" class="form-control" id="ijazah" name="ijazah" required>
                        </div>
                        <div class="form-group">
                            <label for="">Perjanjian Kerjasama</label>
                            <input type="file" class="form-control" id="perjanjian" name="perjanjian" required>
                        </div>
                        <div class="form-group">
                            <label for="">Rekomendasi dari Organisasi</label>
                            <input type="file" class="form-control" id="rekomendasi" name="rekomendasi" required>
                        </div>
                        <div class="form-group">
                            <label for="">Kompetensi yang Diterbitkan oleh Lembaga</label>
                            <input type="file" class="form-control" id="kompetensi" name="kompetensi" required>
                        </div>
                        <div class="form-group">
                            <label for="">Keterangan Persyaratan Tempat</label>
                            <input type="file" class="form-control" id="keterangan" name="keterangan" required>
                        </div>
                        <div class="form-group">
                            <label for="">Rekomendasi SIPP</label>
                            <input type="file" class="form-control" id="sipp" name="sipp" required>
                        </div>
                    <button type="submit" class="btn btn-primary">Simpan Data</button>
                    </form>
                <!--END FORM TAMBAH BARANG-->
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="kontrol" tabindex="-1" aria-labelledby="kontrol" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Upload Data Nomor Kontrol Veteriner</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <!--FORM TAMBAH BARANG-->
                <form method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }} {{ method_field('POST') }}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="">NIK</label>
                            <input type="text" class="form-control" id="nik" name="nik" required>
                        </div>
                        <div class="form-group">
                            <label for="">KTP</label>
                            <input type="file" class="form-control" id="ktp" name="ktp" required>
                        </div>
                        <div class="form-group">
                            <label for="">NIB</label>
                            <input type="file" class="form-control" id="nib" name="nib" required>
                        </div>
                        <div class="form-group">
                            <label for="">SIUP/TDP</label>
                            <input type="file" class="form-control" id="siup" name="siup" required>
                        </div>
                        <div class="form-group">
                            <label for="">NPWP</label>
                            <input type="file" class="form-control" id="npwp" name="npwp" required>
                        </div>
                        <div class="form-group">
                            <label for="">Surat Keterangan Domisili Unit Usaha</label>
                            <input type="file" class="form-control" id="keterangan" name="keterangan" required>
                        </div>
                        <div class="form-group">
                            <label for="">Surat Pernyataan Bermatrai</label>
                            <input type="file" class="form-control" id="pernyataan" name="pernyataan" required>
                        </div>
                        <div class="form-group">
                            <label for="">Bukti Perjanjian Pengelola Usaha</label>
                            <input type="file" class="form-control" id="perjanjian" name="perjanjian" required>
                        </div>
                        <div class="form-group">
                            <label for="">Surat Kuasa</label>
                            <input type="file" class="form-control" id="kuasa" name="kuasa" required>
                        </div>
                    <button type="submit" class="btn btn-primary">Simpan Data</button>
                    </form>
                <!--END FORM TAMBAH BARANG-->
                </div>
            </div>
        </div>
    </div>

    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="{{ asset('plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="{{ asset('plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') }}"></script>
    <!--slimscroll JavaScript -->
    <script src="{{ asset('ampleadmin-minimal/js/jquery.slimscroll.js') }}"></script>
    <!--Wave Effects -->
    <script src="{{ asset('ampleadmin-minimal/js/waves.js') }}"></script>
    <!-- Custom Theme JavaScript -->
    <script src="{{ asset('ampleadmin-minimal/js/custom.min.js') }}"></script>
    <!--Style Switcher -->
    <script src="{{ asset('plugins/bower_components/styleswitcher/jQuery.style.switcher.js') }}"></script>
    <script src="{{ asset('assets/validator/validator.min.js') }}"></script>

    <script>
        function addForm() {
            save_method = "add";
            $('input[name=_method]').val('POST');
            $('#praktek').modal('show');
            $('#praktek form')[0].reset();
        }
        function search() {
            var resultsDiv = document.getElementById("results");
            var userInput = document.getElementById("search-input").value;
            url = "{{ url('search?nik=') }}" + userInput;

            var req = new XMLHttpRequest();
            req.responseType = "json";
            req.open("GET", url, true);
            req.onload = function() {
                var datas = req.response;
                var content = '<div class="table-responsive"><table id="myTable" class="table table-striped"><thead><tr>';
                for (let index = 0; index < datas.length; index++) {
                    const hasil = datas[index];
                    content += "<th>" + hasil.name + "</th>";
                }
                content += '</tr></thead>';
                content += '<tbody><tr>';
                for (let index = 0; index < datas.length; index++) {
                    const hasil = datas[index];
                    if (hasil.keterangan == "proses") {
                        content += '<td bgcolor="orange"><font color="#fff">' + hasil.keterangan + '</font></td>';
                    }else if(hasil.keterangan == "selesai"){
                        content += '<td bgcolor="green"><font color="#fff">' + hasil.keterangan + '</font></td>';
                    }else if(hasil.keterangan == "belum di ambil"){
                        content += '<td bgcolor="blue"><font color="#fff">' + hasil.keterangan + '</font></td>';;
                    }else{
                        content += '<td bgcolor="white"><font color="black">' + hasil.keterangan + '</font></td>';;
                    }
                }
                content += '</tr></tbody>';
                content += '</table></div>';
                resultsDiv.innerHTML = content;
            };
            req.send(null);
        }
        function searchh() {
            var resultsDiv = document.getElementById("resultss");
            var userInput = document.getElementById("search-inputt").value;
            url = "{{ url('search?nik=') }}" + userInput;

            var req = new XMLHttpRequest();
            req.responseType = "json";
            req.open("GET", url, true);
            req.onload = function() {
                var datas = req.response;
                var content = '<div class="table-responsive"><table id="myTable" class="table table-striped"><thead><tr>';
                for (let index = 0; index < datas.length; index++) {
                    const hasil = datas[index];
                    content += "<th>" + hasil.name + "</th>";
                }
                content += '</tr></thead>';
                content += '<tbody><tr>';
                for (let index = 0; index < datas.length; index++) {
                    const hasil = datas[index];
                    if (hasil.keterangan == "proses") {
                        content += '<td bgcolor="orange"><font color="#fff">' + hasil.keterangan + '</font></td>';
                    }else if(hasil.keterangan == "selesai"){
                        content += '<td bgcolor="green"><font color="#fff">' + hasil.keterangan + '</font></td>';
                    }else if(hasil.keterangan == "belum di ambil"){
                        content += '<td bgcolor="blue"><font color="#fff">' + hasil.keterangan + '</font></td>';;
                    }else{
                        content += '<td bgcolor="white"><font color="black">' + hasil.keterangan + '</font></td>';;
                    }
                }
                content += '</tr></tbody>';
                content += '</table></div>';
                resultsDiv.innerHTML = content;
            };
            req.send(null);
        }

        $(function(){

            $('#praktek form').validator().on('submit', function (e) {
                if (!e.isDefaultPrevented()){
                    var id = $('#id').val();
                    url = "{{ url('upload_sip') }}";
                    $.ajax({
                        url : url,
                        type : "POST",
                        enctype: 'multipart/form-data',
                        data: new FormData($("#praktek form")[0]),
                        contentType: false,
                        processData: false,
                        success : function(data) {
                            $('#praktek').modal('hide');
                            swal({
                                title: 'Success!',
                                text: data.message,
                                type: 'success'
                            })
                        },
                        error : function(data){
                            console.log(data.responseJSON);
                            console.log(data);
                            swal({
                                title: 'Oops...',
                                text: 'Format file Harus PDF',
                                type: 'error'
                            })
                        }
                    });
                    return false;
                }
            });

            $('#paramedik form').validator().on('submit', function (e) {
                if (!e.isDefaultPrevented()){
                    var id = $('#id').val();
                    url = "{{ url('upload_sipp') }}";
                    $.ajax({
                        url : url,
                        type : "POST",
                        enctype: 'multipart/form-data',
                        data: new FormData($("#paramedik form")[0]),
                        contentType: false,
                        processData: false,
                        success : function(data) {
                            $('#paramedik').modal('hide');
                            $('#paramedik form')[0].reset();
                            swal({
                                title: 'Success!',
                                text: data.message,
                                type: 'success'
                            })
                        },
                        error : function(data){
                            console.log(data.responseJSON);
                            console.log(data);
                            swal({
                                title: 'Oops...',
                                text: 'Format file Harus PDF',
                                type: 'error'
                            })
                        }
                    });
                    return false;
                }
            });

            $('#kontrol form').validator().on('submit', function (e) {
                if (!e.isDefaultPrevented()){
                    url = "{{ url('upload_nkv') }}";
                    $.ajax({
                        url : url,
                        type : "POST",
                        enctype: 'multipart/form-data',
                        data: new FormData($("#kontrol form")[0]),
                        contentType: false,
                        processData: false,
                        success : function(data) {
                            $('#kontrol').modal('hide');
                            $('#kontrol form')[0].reset();
                            swal({
                                title: 'Success!',
                                text: data.message,
                                type: 'success'
                            })
                        },
                        error : function(data){
                            console.log(data.responseJSON);
                            console.log(data);
                            swal({
                                title: 'Oops...',
                                text: 'Format file Harus PDF',
                                type: 'error'
                            })
                        }
                    });
                    return false;
                }
            });
        });
    </script>
</body>

</html>
