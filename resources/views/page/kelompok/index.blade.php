@extends('layout.app')

@section('title')
    Kelompok | Info Pangan
@endsection

@section('customcss')

@endsection

@section('header')
<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
    <h4 class="page-title">Kelompok</h4> </div>
<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
    <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
    <ol class="breadcrumb">
        <li><a href="#">Master</a></li>
        <li class="active">Kelompok</li>
    </ol>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="panel-heading">
            <h4>Kelompok List
                <a onclick="addForm()" class="btn btn-info pull-right" style="margin-top: -8px;">Add</a>
            </h4>
        </div>
        <div class="white-box">
            <div class="table-responsive">
                <table id="myTable" class="table table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Desa</th>
                            <th>Kecamatan</th>
                            <th>Ketua</th>
                            <th>Telp</th>
                            <th>NIK</th>
                            <th>Bantuan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $datas => $entry)
                        <tr>
                            <td>{{ $datas+1 }}</td>
                            <td>{{ $entry->nama_kelompok }}</td>
                            <td>{{ $entry->id_desa }}</td>
                            <td>{{ $entry->id_kecamatan }}</td>
                            <td>{{ $entry->nama_ketua }}</td>
                            <td>{{ $entry->telp }}</td>
                            <td>{{ $entry->nik }}</td>
                            <td>{{ $entry->bantuan }}</td>
                            <td>
                                <!-- <a onclick="editForm('{{$entry->id}}')" class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-edit"></i> Edit</a> -->
                                <a onclick="deleteData('{{$entry->id}}')" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i> Delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal-form" tabindex="1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="form-contact" method="post" class="form-horizontal" data-toggle="validator" enctype="multipart/form-data">
                {{ csrf_field() }} {{ method_field('POST') }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"> &times; </span>
                    </button>
                    <h3 class="modal-title"></h3>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" id="id" name="id">
                        <div class="form-group">
                            <label for="name" class="col-md-3 control-label">Name</label>
                            <div class="col-md-6">
                                <input type="text" id="name" name="name" class="form-control" autofocus required>
                                <span class="help-block with-errors"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="kecamatan" class="col-md-3 control-label">Kecamatan</label>
                            <div class="col-md-6">
                                <select id="kecamatan" name="kecamatan" class="form-control">
                                    @foreach($kecamatan as $datas => $data)
                                    <option value="{{$data->id}}">{{$data->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="desa" class="col-md-3 control-label">Desa</label>
                            <div class="col-md-6">
                                <select id="desa" name="desa" class="form-control">
                                    @foreach($desa as $datas => $data)
                                    <option value="{{$data->id}}">{{$data->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ketua" class="col-md-3 control-label">Ketua</label>
                            <div class="col-md-6">
                                <input type="text" id="ketua" name="ketua" class="form-control" autofocus required>
                                <span class="help-block with-errors"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="telp" class="col-md-3 control-label">Telp</label>
                            <div class="col-md-6">
                                <input type="text" id="telp" name="telp" class="form-control" autofocus required>
                                <span class="help-block with-errors"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nik" class="col-md-3 control-label">NIK</label>
                            <div class="col-md-6">
                                <input type="text" id="nik" name="nik" class="form-control" autofocus required>
                                <span class="help-block with-errors"></span>
                            </div>
                        </div>
                    </div>

                    <h3 class="box-title">Bantuan
                    <button class="add_form_field btn btn-primary">Add New Field &nbsp;
                            <span style="font-weight:bold;">+ </span>
                    </button></h3>
                    <hr class="m-t-0 m-b-40">
                    <div class="container1 row">
                        <div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Item</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" id="item[]" name="item[]"> </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Jumlah</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" id="jumlah[]" name="jumlah[]"> </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <!-- <label for="bantuan" class="col-md-3 control-label">Bantuan</label> -->
                        <!-- <div class="container1 col-md-6"> -->
                            <!-- <button class="add_form_field">Add New Field &nbsp;
                            <span style="font-size:16px; font-weight:bold;">+ </span>
                            </button>
                            <div><input type="text" name="mytext[]"></div> -->
                        <!-- </div> -->
                        <!-- <div class="col-md-6">
                            <input type="text" id="bantuan" name="bantuan" class="form-control" autofocus required>
                            <span class="help-block with-errors"></span>
                        </div> -->

                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-save">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>

            </form>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
        var max_fields = 5;
        var wrapper = $(".container1");
        var add_button = $(".add_form_field");

        var x = 1;
        $(add_button).click(function(e) {
            e.preventDefault();
            if (x < max_fields) {
                x++;
                $(wrapper).append('<div><div class="col-md-6"><div class="form-group"><label class="control-label col-md-3">Item</label><div class="col-md-9"><input type="text" class="form-control" id="item[]" name="item[]"> </div></div></div><div class="col-md-3"><div class="form-group"><label class="control-label col-md-3">Jumlah</label><div class="col-md-9"><input type="text" class="form-control" id="jumlah[]" name="jumlah[]"></div></div></div><button class="delete btn btn-danger">Hapus</button><p><br></div>'); //add input box
                // $(wrapper).append('<div><input type="text" name="mytext[]"/><a href="#" class="delete">Delete</a></div>'); //add input box
            } else {
                alert('Maksimal 5 form')
            }
        });

        $(wrapper).on("click", ".delete", function(e) {
            e.preventDefault();
            $(this).parent('div').remove();
            x--;
        })

    });
    function addForm() {
        save_method = "add";
        $('input[name=_method]').val('POST');
        $('#modal-form').modal('show');
        $('#modal-form form')[0].reset();
        $('.modal-title').text('Add Kelompok');
    }
    function showData(id) {
        //alert("Show Data Jabatan "+ id);
        //$('input[name=_method]').val('PATCH');
        //$('#modal-showeuy form')[0].reset();
        $.ajax({
            url: "{{ url('kelompok') }}" + '/' + id + "/edit",
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                //$('#modal-showeuy').modal('show');
                //$('.modal-title').text('Info Jabatan');

                // $('#id').val(data.id);
                // $('#nama_jabatan').val(data.nama_jabatan);
                // $('#kode').val(data.kode);
                alert("Name : "+ data.name + '<br>' +
                "Username : "+ data.username +"<br>"
                );
            },
            error : function() {
                alert("Nothing Data");
            }
        });
    }

    function editForm(id) {
        save_method = 'edit';
        $('input[name=_method]').val('PATCH');
        $('#modal-form form')[0].reset();
        $.ajax({
            url: "{{ url('kelompok') }}" + '/' + id + "/edit",
            type: "GET",
            dataType: "JSON",
            success: function(data_edit) {
                $('#modal-form').modal('show');
                $('.modal-title').text('Edit User');

                $('#id').val(data_edit.id);
                $('#name').val(data_edit.nama_kelompok);
                $('#kecamatan').val(data_edit.id_kecamatan);
                $('#desa').val(data_edit.id_desa);
                $('#ketua').val(data_edit.nama_ketua);
                $('#telp').val(data_edit.telp);
                $('#nik').val(data_edit.nik);
                $('#bantuan').val(data_edit.bantuan);

            },
            error : function() {
                alert("Nothing Data euyy...!!");
            }
        });
    }

    function deleteData(id){
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Yes, delete it!'
        }).then(function () {
            $.ajax({
                url : "{{ url('kelompok') }}" + '/' + id,
                type : "POST",
                data : {'_method' : 'DELETE', '_token' : '{{ csrf_token() }}'},
                success : function(data) {
                    swal("Good job!", data.message, "success").then(function(){
                        location.reload();
                    });
                },
                error : function () {
                    swal({
                        title: 'Oops...',
                        text: data.message,
                        type: 'error',
                        timer: '1500'
                    })
                }
            });
        });
    }

    $(function(){
        $('#modal-form form').validator().on('submit', function (e) {
            if (!e.isDefaultPrevented()){
                var id = $('#id').val();
                if (save_method == 'add') url = "{{ url('kelompok') }}";
                else url = "{{ url('kelompok') . '/' }}" + id;

                $.ajax({
                    url : url,
                    type : "POST",
        //                        data : $('#modal-form form').serialize(),
                    data: new FormData($("#modal-form form")[0]),
                    contentType: false,
                    processData: false,
                    success : function(data) {
                        $('#modal-form form')[0].reset();
                        if (data.success == true) {
                            swal("Good job!", data.message, "success").then(function(){
                                location.reload();
                            });
                        }else{
                            swal("Oops...", data.message, "error").then(function(){
                                location.reload();
                            });
                        }
                    },
                    error : function(data){
                        console.log(data);
                        swal({
                            title: 'Oops...',
                            text: data.message,
                            type: 'error',
                            timer: '1500'
                        })
                    }
                });
                return false;
            }
        });
    });
</script>
@endpush
