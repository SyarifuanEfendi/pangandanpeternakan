@extends('layout.app')

@section('title')
    Laporan | Info Pangan
@endsection

@section('customcss')

@endsection

@section('header')
<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
    <h4 class="page-title">Laporan</h4> </div>
<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
    <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
    <ol class="breadcrumb">
        <li class="">Laporan</li>
        <li class="active">Detail</li>
    </ol>
</div>
@endsection

@section('content')
<!-- .row -->
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            @foreach($data as $datas)
            <h3 class="box-title m-b-0">Kelompok Tani</h3>
            <p class="text-muted m-b-30 font-13">Data Kelompok Tani</p>
            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tanggal Input</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" value="{{ $datas->tanggal }}" disabled></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nama Kelompok</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" value="{{ $datas->nama_kelompok }}" disabled></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Jumlah Anggota</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" value="{{ $datas->jumlah_anggota }}" disabled></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Desa</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" value="{{ $datas->desa }}" disabled></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Kecamatan</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" value="{{ $datas->kecamatan }}" disabled></div>
                        </div>
                    </form>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <form role="form" class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="example-input-small">Ketua</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" value="{{ $datas->nama_ketua }}" disabled></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="example-input-small">No HP</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" value="{{ $datas->telp }}" disabled></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="example-input-small">nik</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" value="{{ $datas->nik }}" disabled></div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="example-input-small">Bantuan</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" value="{{ $datas->bantuan }}" disabled></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="example-input-small">Pemanfaatan</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" value="{{ $datas->pemanfaatan }}" disabled></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="example-input-small">Hasil</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" value="{{ $datas->hasil }}" disabled></div>
                        </div>
                    </form>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title m-b-0">Bantuan</h3>
            <p class="text-muted m-b-30 font-13">Data Bantuan untuk Kelompok Tani</p>
            <div class="row">
            @foreach($bantuan as $datas)
                <form class="form-horizontal" role="form">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Item</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" value="{{ $datas->nama }}" disabled> </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Jumlah</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" value="{{ $datas->jumlah }}" disabled> </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Jumlah Manfaat</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" value="{{ $datas->jumlah_manfaat }}" disabled> </div>
                        </div>
                    </div>
                </form>
            @endforeach
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
<div class="row">
    <div class="col-md-6">
        <div class="white-box">
            <h3 class="box-title m-b-0">Pemanfaatan</h3>
            <p class="text-muted m-b-30 font-13"> Foto-Foto Pemanfaatan</p>
            @if(count($pemanfaat)>0)
                @foreach($pemanfaat as $data)
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <form>
                                <div class="form-group">
                                    <img src="{{ asset('file/laporanpemanfaatan/'.$data->foto) }}" width="750px" >
                                </div>
                            </form>
                        </div>
                    </div>
                @endforeach
            @else
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    Belum Upload Foto
                </div>
            </div>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="white-box">
            <h3 class="box-title m-b-0">Hasil Panen</h3>
            <p class="text-muted m-b-30 font-13"> Foto-Foto Hasil Panen </p>
            @if(count($hasil)>0)
                @foreach($hasil as $data)
                    <form class="form-horizontal">
                        <div class="form-group">
                            <img src="{{ asset('file/laporanhasil/'.$data->foto) }}" width="750px">
                        </div>
                    </form>
                @endforeach
            @else
                Belum Upload Foto
            @endif
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script>
</script>
@endpush
