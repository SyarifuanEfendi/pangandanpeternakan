@extends('layout.app')

@section('title')
    Laporan | Info Pangan
@endsection

@section('customcss')

@endsection

@section('header')
<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
    <h4 class="page-title">Laporan</h4> </div>
<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
    <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
    <ol class="breadcrumb">
        <li class="active">Laporan</li>
    </ol>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="panel-heading">
            <h4>Laporan List
                @if( Auth::user()->aplikasi == 1 && Auth::user()->role == 'editor')
                <a onclick="addForm()" class="btn btn-info pull-right" style="margin-top: -8px;">Add</a>
                @else
                @endif
            </h4>
        </div>
        <div class="white-box">
            <div class="table-responsive">
                <table id="myTable" class="table table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kelompok</th>
                            <th>Tanggal</th>
                            <th>Jumlah Anggota</th>
                            <th>Pemanfaatan</th>
                            <th>hasil</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $datas => $entry)
                        <tr>
                            <td>{{ $datas+1 }}</td>
                            <td>{{ $entry->id_kelompok }}</td>
                            <td>{{ $entry->tanggal }}</td>
                            <td>{{ $entry->jumlah_anggota }}</td>
                            <td>{{ $entry->pemanfaatan }}</td>
                            <td>{{ $entry->hasil }}</td>
                            <td>
                            @if( Auth::user()->aplikasi == 1 && Auth::user()->role == 'editor')
                                <a href="{{ url('laporan-user/'.$entry->id.'/detail') }}" class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-eye-open"></i> Detail</a>
                                <a onclick="deleteData('{{$entry->id}}')" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i> Delete</a>
                            @else
                                <a href="{{ url('laporan/'.$entry->id.'/detail') }}" class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-eye-open"></i> Detail</a>
                            @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal-form" tabindex="1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="form-contact" method="post" class="form-horizontal" data-toggle="validator" enctype="multipart/form-data">
                {{ csrf_field() }} {{ method_field('POST') }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"> &times; </span>
                    </button>
                    <h3 class="modal-title"></h3>
                </div>

                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    <div class="form-group">
                        <label for="kelompok" class="col-md-3 control-label">Kelompok</label>
                        <div class="col-md-6">
                            <select id="kelompok" name="kelompok" class="form-control">
                                @foreach($kelompok as $datas => $data)
                                <option value="{{$data->id}}">{{$data->nama_kelompok}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="jumlah" class="col-md-3 control-label">Jumlah Anggota</label>
                        <div class="col-md-6">
                            <input type="text" id="jumlah" name="jumlah" class="form-control" autofocus required>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pemanfaatan" class="col-md-3 control-label">Pemanfaatan</label>
                        <div class="col-md-6">
                            <input type="text" id="pemanfaatan" name="pemanfaatan" class="form-control" autofocus required>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="hasil" class="col-md-3 control-label">Hasil</label>
                        <div class="col-md-6">
                            <input type="text" id="hasil" name="hasil" class="form-control" autofocus required>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="hasil" class="col-md-3 control-label">Foto Pemanfaatan</label>
                        <div class="col-md-6">
                            <input type="file" name="imagemanfaatan[]" id="imagemanfaatan" multiple class="form-control" accept="image/*" required>
                            @if ($errors->has('files'))
                                @foreach ($errors->get('files') as $error)
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $error }}</strong>
                                </span>
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="hasil" class="col-md-3 control-label">Foto Hasil</label>
                        <div class="col-md-6">
                            <input type="file" name="imagehasil[]" id="imagehasil" multiple class="form-control" accept="image/*" required>
                            @if ($errors->has('files'))
                                @foreach ($errors->get('files') as $error)
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $error }}</strong>
                                </span>
                                @endforeach
                            @endif
                        </div>
                    </div>

                    <h3 class="box-title">Bantuan
                    </button></h3>
                    <hr class="m-t-0 m-b-40">
                    <div class="container1 row">
                        <div>
                            @foreach($bantuan as $datas => $data)
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Item</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" value="{{ $data->nama }}" disabled> </div>
                                            <input type="hidden" class="form-control" id="item[]" name="item[]" value="{{ $data->nama }}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Jumlah</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" id="jumlahh[]" name="jumlahh[]" value="{{ $data->jumlah }}"> </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-save">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>

            </form>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
    });
    function addForm() {
        save_method = "add";
        $('input[name=_method]').val('POST');
        $('#modal-form').modal('show');
        $('#modal-form form')[0].reset();
        $('.modal-title').text('Add Laporan');
    }
    function deleteData(id){
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Yes, delete it!'
        }).then(function () {
            $.ajax({
                url : "{{ url('laporan-user') }}" + '/' + id,
                type : "POST",
                data : {'_method' : 'DELETE', '_token' : '{{ csrf_token() }}'},
                success : function(data) {
                    swal("Good job!", data.message, "success").then(function(){
                        location.reload();
                    });
                },
                error : function () {
                    swal({
                        title: 'Oops...',
                        text: data.message,
                        type: 'error',
                        timer: '1500'
                    })
                }
            });
        });
    }
    $('#form-contact').submit(function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        let TotalFilesimage = $('#imagemanfaatan')[0].files.length; //Total files
        let TotalFileshasil = $('#imagehasil')[0].files.length; //Total files
        let imagemanfaatan = $('#imagemanfaatan')[0];
        let imagehasil = $('#imagehasil')[0];
        for (let i = 0; i < TotalFilesimage; i++) {
            formData.append('imagemanfaatan' + i, imagemanfaatan.files[i]);
        }
        for (let i = 0; i < TotalFileshasil; i++) {
            formData.append('imagehasil' + i, imagehasil.files[i]);
        }
        formData.append('TotalFilesimage', TotalFilesimage);
        formData.append('TotalFileshasil', TotalFileshasil);
        console.log(formData);
        $.ajax({
            type:'POST',
            url: "{{ url('laporan-user')}}",
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: (data) => {
                $('#modal-form').modal('hide');
                $('#modal-form form')[0].reset();
                swal("Good job!", data.message, "success").then(function(){
                    location.reload();
                });
            },
            error: function(data){
                console.log(data);
                alert(data.responseJSON.errors.files[0]);
            }
        });
    });
    </script>
@endpush
