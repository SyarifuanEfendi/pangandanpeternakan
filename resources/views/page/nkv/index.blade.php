@extends('layout.app')

@section('title')
    Nkv | Izin Praktek
@endsection

@section('customcss')

@endsection

@section('header')
<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
    <h4 class="page-title">Desa</h4> </div>
<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
    <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
    <ol class="breadcrumb">
        <li><a href="#">Master</a></li>
        <li class="active">Desa</li>
    </ol>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="panel-heading">
            <h4>NKV List
            </h4>
        </div>
        <div class="white-box">
            <div class="table-responsive">
                <table id="myTable" class="table table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nik</th>
                            <th>Status</th>
                            <th>Proses</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $datas => $entry)
                        <tr>
                            <td>{{ $datas+1 }}</td>
                            <td>{{ $entry->nik }}</td>
                            <td>
                                <b>{{ $entry->status }} </b>
                            </td>
                            <td>
                                @if($entry->status == "proses")
                                    <a onclick="prosesData('{{$entry->id}}')" class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-check"></i> Berkas Bisa di Ambil</a>
                                @elseif( $entry->status == "belum di ambil")
                                    <a onclick="prosesData('{{$entry->id}}')" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-check"></i> Berkas Sudah di Ambil</a>
                                @else
                                    <a class="btn btn-success btn-xs"><i class="glyphicon glyphicon-check" disabled></i> Berkas Selesai</a>
                                @endif

                            </td>
                            <td>
                                <a onclick="downloadData('{{$entry->id}}')" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-download-alt"></i> Download</a>
                                <!-- <a onclick="editForm('{{$entry->id}}')" class="btn btn-primary btn-xs"><i class="glyphicon glyphicon-edit"></i> Edit</a> -->
                                <a onclick="deleteData('{{$entry->id}}')" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i> Delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script>
    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
    $(document).ready(function() {
        $('#myTable').DataTable();
    });
    function addForm() {
        save_method = "add";
        $('input[name=_method]').val('POST');
        $('#modal-form').modal('show');
        $('#modal-form form')[0].reset();
        $('.modal-title').text('Add Subsystem User');
    }
    function prosesData(id){
        swal({
            title: 'Apakah kamu ingin update data ini?',
            type: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Yes, Process it!'
        }).then(function () {
            $.ajax({
                url : "{{ url('nkv/proses') }}" + '/' + id,
                type : "GET",
                data : {'_method' : 'GET', "_token": "{{ csrf_token() }}",},
                success : function(data) {
                    swal("Good job!", data.message, "success").then(function(){
                        location.reload();
                    });
                },
                error : function (data) {
                    swal({
                        title: 'Oops...',
                        text: data.message,
                        type: 'error',
                        timer: '1500'
                    })
                }
            });
        });
    }
    function downloadData(id){
        swal({
            title: 'Apakah kamu ingin mendownload file ini?',
            type: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Yes, download it!'
        }).then(function () {
            $.ajax({
                url : "{{ url('nkv/download') }}" + '/' + id,
                type : "GET",
                data : {'_method' : 'GET', "_token": "{{ csrf_token() }}",},
                success : function(response) {
                    var blob = new Blob([response]);
                    var link = document.createElement('a');
                    link.href = window.URL.createObjectURL(blob);
                    link.download = "arsip_berkas.zip";
                    link.click();
                    // swal({
                    //     title: 'Success!',
                    //     text: data.message,
                    //     type: 'success',
                    //     timer: '1500'
                    // })
                },
                error : function (data) {
                    swal({
                        title: 'Oops...',
                        text: data.message,
                        type: 'error',
                        timer: '1500'
                    })
                }
            });
        });
    }
    </script>
@endpush
