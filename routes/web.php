<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\DesaController;
use App\Http\Controllers\KecamatanController;
use App\Http\Controllers\LaporanController;
use App\Http\Controllers\KelompokController;
use App\Http\Controllers\SipController;
use App\Http\Controllers\SippController;
use App\Http\Controllers\NkvController;
use App\Http\Controllers\GrafikController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\ArtikelController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','App\Http\Controllers\FrontendController@index');

Route::get('login', 'App\Http\Controllers\AuthController@index')->name('login');
// Route::get('register', 'App\Http\Controllers\AuthController@register')->name('register');
Route::post('proses_login', 'App\Http\Controllers\AuthController@proses_login')->name('proses_login');
Route::get('logout', 'App\Http\Controllers\AuthController@logout')->name('logout');
Route::post('upload_sip','App\Http\Controllers\SipController@store');
Route::post('upload_sipp','App\Http\Controllers\SippController@store');
Route::post('upload_nkv','App\Http\Controllers\NkvController@store');
Route::get('artikel_list','App\Http\Controllers\FrontendController@index');
Route::get('alur','App\Http\Controllers\FrontendController@alur');
Route::get('artikel_detail/{id}', 'App\Http\Controllers\ArtikelController@detail');
Route::get('search','App\Http\Controllers\FrontendController@search')->name('searchFriend');

Route::group(['middleware' => ['auth']], function () {
    Route::group(['middleware' => ['cek_login:admin']], function () {
        Route::resource('admin', UsersController::class);
        Route::resource('desa', DesaController::class);
        Route::resource('kecamatan', KecamatanController::class);
        Route::get('api/kecamatan', 'App\Http\Controllers\KecamatanController@apiKecamatan');
        Route::resource('laporan', LaporanController::class);
        Route::get('laporan/{id}/detail', 'App\Http\Controllers\LaporanController@detail');
        Route::resource('kelompok', KelompokController::class);
        Route::resource('sip', SipController::class);
        Route::resource('siipp', SippController::class);
        Route::resource('nkv', NkvController::class);
        Route::resource('grafik', GrafikController::class);
        Route::resource('artikel', ArtikelController::class);
        Route::get('/nkv/download/{file}', 'App\Http\Controllers\NkvController@download');
        Route::get('/nkv/proses/{file}', 'App\Http\Controllers\NkvController@proses');
        Route::get('/sip/download/{file}', 'App\Http\Controllers\SipController@download');
        Route::get('/sip/proses/{file}', 'App\Http\Controllers\SipController@proses');
        Route::get('/sipp/download/{file}', 'App\Http\Controllers\SippController@download');
        Route::get('/sipp/proses/{file}', 'App\Http\Controllers\SippController@proses');
    });
    Route::group(['middleware' => ['cek_login:editor']], function () {
        Route::resource('laporan-user', LaporanController::class);
        Route::get('laporan-user/{id}/detail', 'App\Http\Controllers\LaporanController@detail');
        Route::resource('editor', UsersController::class);
    });
});
